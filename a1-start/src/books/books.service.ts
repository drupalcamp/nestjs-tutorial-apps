import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from './book.entity';
import { BooksRepository } from './books.repository';
import { CreateBookDTO } from './dto/create-book.dto';

@Injectable()
export class BooksService {
  constructor(
    @InjectRepository(BooksRepository)
    private booksRepository: BooksRepository,
  ) {}

  public async createBook(createBookDto: CreateBookDTO): Promise<Book> {
    return await this.booksRepository.createBook(createBookDto);
  }

  public async getBooks(): Promise<Book[]> {
    return await this.booksRepository.find();
  }

  public async getBook(bookId: number): Promise<Book> {
    const foundBook = await this.booksRepository.findOne(bookId);
    if (!foundBook) {
      throw new NotFoundException('Book not found!');
    }

    return foundBook;
  }

  public async editBook(
    bookId: number,
    createBookDto: CreateBookDTO,
  ): Promise<Book> {
    const book = await this.booksRepository.findOne(bookId);
    if (!book) {
      throw new NotFoundException('Book not found');
    }
    return this.booksRepository.editBook(createBookDto, book);
  }
  public async deleteBook(bookId: number): Promise<void> {
    await this.booksRepository.delete(bookId);
  }
}
