import { EntityRepository, Repository, MongoRepository } from 'typeorm';
import { Book } from './book.entity';
import { CreateBookDTO } from './dto/create-book.dto';

@EntityRepository(Book)
export class BooksRepository extends MongoRepository<Book> {
  public async createBook(createBookDto: CreateBookDTO): Promise<Book> {
    const { title, description, author1, author2, price } = createBookDto;

    const book = new Book();
    book.title = title;
    book.description = description;
    book.author1 = author1;
    book.author2 = author2;
    book.price = price;
    await book.save();
    return book;
  }

  public async editBook(
    createBookDto: CreateBookDTO,
    book: Book,
  ): Promise<Book> {
    const { title, description, author1, author2, price } = createBookDto;

    book.title = title;
    book.description = description;
    book.author1 = author1;
    book.author2 = author2;
    book.price = price;
    await book.save();
    return book;
  }
}
