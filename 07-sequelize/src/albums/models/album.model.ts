import { Column, Model, Table } from 'sequelize-typescript';

@Table
export class Album extends Model {
  @Column
  title: string;

  @Column
  user: string;

}
