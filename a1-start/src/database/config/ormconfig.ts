import { join } from 'path';

export function ormConfig(): any {
  return {
    // This type is not working 
    // type: 'mongodb',
    // type: process.env.DATABASE_TYPE,

    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT),
    // username: process.env.DATABASE_USERNAME,
    // password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    synchronize: true,
    entities: [join('/', '**', '*.entity.{ts,js}')],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };
}
