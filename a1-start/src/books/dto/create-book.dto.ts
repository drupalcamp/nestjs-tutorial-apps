import { IsString } from 'class-validator';

export class CreateBookDTO {
  @IsString()
  title: string;

  @IsString()
  description: string;

  @IsString()
  author1: string;

  @IsString()
  author2: string;

  @IsString()
  price: string;
}
