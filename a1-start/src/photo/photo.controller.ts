import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Photo } from './photo.entity';
import { MongoRepository, ObjectID } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Controller('photo')
export class PhotoController {
  constructor(
    @InjectRepository(Photo)
    private readonly photoRepository: MongoRepository<Photo>,
  ) {}

  @Get()
  async findAll(): Promise<Photo[]> {
    return this.photoRepository.find();
  }

  @Get('/:id')
  async findOne(@Param('id') id): Promise<Photo> {
    const photo = await this.photoRepository.findOne(id);
    if (!photo) {
      throw new NotFoundException();
    }
    return photo;
  }

  @Post('/create')
  async createPhoto(@Body() photo: Partial<Photo>): Promise<Photo> {
    if (!photo || !photo.name || !photo.filename) {
      throw new BadRequestException(`A photo mush have name and filename.`);
    }
    return await this.photoRepository.save(new Photo(photo));
  }

  @Put('/edit/:id')
  @HttpCode(204)
  async updatePhoto(
    @Param('id') id,
    @Body() photo: Partial<Photo>,
  ): Promise<void> {
    // Check if entity exists
    // const exists = ObjectID.isValid(id) && (await this.photoRepository.findOne(id));
    const exists = (await this.photoRepository.findOne(id));
    if (!exists) {
      throw new NotFoundException();
    }
    await this.photoRepository.update(id, photo);
  }
}
