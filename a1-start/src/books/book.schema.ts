import { EntitySchema } from 'typeorm';
import { Book } from './book.entity';

export const BooksRepository = new EntitySchema<Book>({
  name: 'Book',
  target: Book,
  columns: {
    id: {
      type: Number,
      primary: true,
      generated: true,
    },
    title: {
      type: String,
    },
    description: {
      type: String,
    },
    author1: {
      type: String,
    },
    author2: {
      type: String,
    },
    price: {
      type: String,
    },
  },
  // relations: {
  //   photos: {
  //     type: 'one-to-many',
  //     target: 'Photo', // the name of the PhotoSchema
  //   },
  // },
});
