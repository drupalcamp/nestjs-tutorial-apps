import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductModule } from './product/product.module';
import { BooksModule } from './books/books.module';
import { PhotoModule } from './photo/photo.module';
import { ConfigModule } from '@nestjs/config';
import { ormConfig } from './database/config/ormconfig';
import { join } from 'path';

@Module({
  imports: [
  //   TypeOrmModule.forRoot({
  //     type: 'mysql',
  //     host: 'localhost',
  //     port: 3306,
  //     username: 'root',
  //     password: 'gungwang',
  //     database: 'testnest',
  //     autoLoadEntities: true,
  //     synchronize: true,
  //     entities: [join(__dirname, '**', '*.entity.{ts,js}')],
  //   }),

    // Not working in .env file
    // ConfigModule.forRoot({ isGlobal: true }),
    // TypeOrmModule.forRoot(ormConfig()),
    
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: 'localhost',
      port: 27017,
      database: 'testnest',
      entities: [join(__dirname, '**', '*.entity.{ts,js}')],
      synchronize: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }),
    // ProductModule,
    // BooksModule,
    PhotoModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
