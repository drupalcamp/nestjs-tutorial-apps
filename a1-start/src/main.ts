import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // const port = parseInt(process.env.SERVER_PORT);
  await app.listen(3000);
  // console.log(process.env);
}
bootstrap();
