import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';

@Entity('photo')
export class Photo {
  @ObjectIdColumn() id: ObjectID;
  @Column() name: string;
  @Column() description: string;
  @Column() filename: string;
  @Column() isPublished: boolean;

  constructor(photo?: Partial<Photo>) {
    Object.assign(this, photo);
  }
}
