import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { Book } from './book.entity';
import { BooksService } from './books.service';
import { CreateBookDTO } from './dto/create-book.dto';

@Controller('books')
export class BooksController {
  // Constructor, BooksService as DI.
  constructor(private bookService: BooksService) {}

  @Post('create')
  public async createBook(@Body() createBookDto: CreateBookDTO): Promise<Book> {
    const book = await this.bookService.createBook(createBookDto);
    return book;
  }

  @Get('all')
  public async getBooks(): Promise<Book[]> {
    const books = await this.bookService.getBooks();
    return books;
  }

  @Get('/:bookId')
  public async getBook(@Param('bookId') booidId: number) {
    return await this.bookService.getBook(booidId);
  }

  @Patch('/edit/:bookId')
  public async editBook(
    @Body() createBookDto: CreateBookDTO,
    @Param('bookId') bookId: number,
  ): Promise<Book> {
    const book = await this.bookService.editBook(bookId, createBookDto);
    return book;
  }
  @Delete('/delete/:bookId')
  public async deleteBook(@Param('bookId') bookId: number) {
    const deletedBook = await this.bookService.deleteBook(bookId);
    return deletedBook;
  }

}
